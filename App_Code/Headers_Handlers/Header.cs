﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

/// <summary>
/// Descripción breve de HeaderHandler
/// </summary>
public class Header : SoapHeader
{
    private String token;

    public Header()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public string Token
    {
        get
        {
            return token;
        }

        set
        {
            token = value;
        }
    }
}