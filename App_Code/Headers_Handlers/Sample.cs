﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Sample
/// </summary>
public class Sample
{
    private String sampleName;
    private int sampleCount;
    private DateTime sampleCreated;

    public int SampleCount
    {
        get
        {
            return sampleCount;
        }

        set
        {
            sampleCount = value;
        }
    }

    public DateTime SampleCreated
    {
        get
        {
            return sampleCreated;
        }

        set
        {
            sampleCreated = value;
        }
    }

    public string SampleName
    {
        get
        {
            return sampleName;
        }

        set
        {
            sampleName = value;
        }
    }

    public Sample()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }


}