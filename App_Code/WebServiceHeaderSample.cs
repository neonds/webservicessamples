﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

/// <summary>
/// Descripción breve de WebServiceHeaderSample
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceHeaderSample : System.Web.Services.WebService
{
    public Header headerAttribute;

    public WebServiceHeaderSample()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    [SoapHeader("headerAttribute")]
    public Sample TestMessage(Sample sample)
    {

        sample.SampleName = headerAttribute.Token;
        return sample;
    }

}
